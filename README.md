# Première websocket

je voulais démistifier rapidement le fonctionnements des webSockets, c'est chose faite, à une petit échelle

## TODO

1. permettre de nommer les clients qui se connecte
    1. inclure le nom choisi par le client dans les messages
1. sauvegarder les messages côté serveur
1. notion de conversation
1. passer au webRTC

### INSTALLATION

1. cloner le dépot https://gitlab.com/portfolio_jmj/node/websocket.git
1. lancer le serveur dans server/server.js
1. executer les clients client/client.html client2/client.html