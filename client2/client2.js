console.log('je suis le client 2');

const button = buttonSend;
const buttonC = buttonClose;

const message = inputMessage;
const table = tableMessage;

const messages = []

console.log(messages);

// creation du webSocket
const webSocket = new WebSocket('ws://192.168.176.163:3000');

buttonC.onclick = () => {
    webSocket.close(1001 , 'client - 1, a fermer la connection');
}

button.onclick = () => {
    const value = message.value;


    const messageSend = {
        'client'    : 'client2',
        'date'      : new Date(Date.now()).toLocaleString(),
        'content'   : value
    }
    messages.push(messageSend);

    // pour envoyer un message
    webSocket.send(JSON.stringify(messageSend));
}

// pour recevoir un message
webSocket.onmessage = ((evt) => {
    const data = JSON.parse(evt.data);
    //console.log('websocket message incoming :',JSON.parse(evt.data))
    createLine(data.message);    
    messages.push(data.message);
})

webSocket.onerror = ((evt) => {
    console.log('une erreur est survenue : ', evt);
    document.body.textContent = '<h1> Une erreur est survenue recharger la page </h1>'
})


function createLine(obj) {
    const tr = document.createElement('tr');
    const td1 = document.createElement('td');
    td1.textContent = obj.date;
    tr.appendChild(td1);
    const td2 = document.createElement('td');
    td2.textContent = obj.client;
    tr.appendChild(td2);
    const td3 = document.createElement('td');
    td3.textContent = obj.content;
    tr.appendChild(td3);
    table.appendChild(tr);
}