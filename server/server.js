const Koa = require('koa');
const app = new Koa();

const server = app.listen(3000);

WebSocket = require("ws"),
wsServer = new WebSocket.Server({ server });

console.log('koaServer + webSocket ok');

const messages = [];

//#region WebSocket

//when a websocket connection is established
wsServer.on('connection', (webSocketClient) => {
    //send feedback to the incoming connection
    //webSocketClient.send('{ "connection" : "ok"}');

    //when a message is received
    webSocketClient.on('message', (message) => {
        //console.log('server message incoming : ', message);
        messages.push(message);
        //for each websocket client
        wsServer
        .clients
        .forEach( client => {
            //send the client the current message
            client.send(`{ "message" : ${message} }`);
        });
    });
});

wsServer.on('close', () => {
    console.log('un client est partie');
})



/*
//when a message is received
webSocketClient.on('message', (message) => {
    console.log('server message incoming');
    //for each websocket client
    wsServer
    .clients
    .forEach( client => {
        //send the client the current message
        client.send(`{ "message" : ${message} }`);
    });
});
*/

//#region 